﻿using System;

namespace Talk.Data.Views
{
    /// <summary>
    /// Uživatel.
    /// </summary>
    public interface IUserDataView
    {
        /// <summary>
        /// Id uživatele.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Uživatelské jméno.
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        string Email { get; set; }
    }
}
