﻿using System;

namespace Talk.Data.Views
{
    /// <summary>
    /// Zpráva.
    /// </summary>
    public class MessageDataView : IMessageDataView
    {
        /// <summary>
        /// Id zprávy.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Datum a čas vytvoření zprávy.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Text zprávy.
        /// </summary>
        public string MessageText { get; set; }

        /// <summary>
        /// Id uživatele, který odeslal.
        /// </summary>
        public int UserSentId { get; set; }

        /// <summary>
        /// Id uživatele, který obdržel.
        /// </summary>
        public int UserReceivedId { get; set; }
    }
}
