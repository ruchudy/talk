﻿using System;

namespace Talk.Data.Views
{
    /// <summary>
    /// Uživatel.
    /// </summary>
    public class UserDataView : IUserDataView
    {
        /// <summary>
        /// Id uživatele.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Uživatelské jméno.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }
    }
}
