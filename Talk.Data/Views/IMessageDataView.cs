﻿using System;

namespace Talk.Data.Views
{
    /// <summary>
    /// Zpráva.
    /// </summary>
    public interface IMessageDataView
    {
        /// <summary>
        /// Id zprávy.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Datum a čas vytvoření zprávy.
        /// </summary>
        DateTime DateCreated { get; set; }

        /// <summary>
        /// Text zprávy.
        /// </summary>
        string MessageText { get; set; }

        /// <summary>
        /// Uživatel, který odeslal.
        /// </summary>
        int UserSentId { get; set; }

        /// <summary>
        /// Uživatel, který obdržel.
        /// </summary>
        int UserReceivedId { get; set; }
    }
}
