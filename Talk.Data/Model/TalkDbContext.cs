﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Talk.Data.Model
{
    public class TalkDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        // Constructor
        public TalkDbContext(DbContextOptions<TalkDbContext> options)
            : base(options)
        {
        }

        // Entities
        public DbSet<Message> Messages { get; set; }

        // Creating
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new Maping.MessageMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
