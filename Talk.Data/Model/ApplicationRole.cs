﻿using Microsoft.AspNetCore.Identity;

namespace Talk.Data.Model
{
    public class ApplicationRole : IdentityRole<int>
    {
    }
}
