﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Talk.Data.Model.Maping
{
    public class MessageMap : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(s => s.Id).HasName("PK_Message");
            builder.Property(s => s.Id).IsRequired().HasColumnName("Id").HasColumnType("int").ValueGeneratedOnAdd();
            builder.Property(s => s.DateCreated).IsRequired().HasColumnName("Created").HasColumnType("datetime2");
            builder.Property(s => s.Group).IsRequired().HasColumnName("Group").HasColumnType("varchar(25)").HasMaxLength(25);
            builder.Property(s => s.MessageText).IsRequired().HasColumnName("Text").HasColumnType("nvarchar(1024)").HasMaxLength(1024);

            builder.HasOne(s => s.UserSent).WithMany(r => r.MessagesSent).IsRequired()
                .HasForeignKey(p => p.UserSentId).HasConstraintName("FK_Message_UserSent").OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(s => s.UserReceived).WithMany(r => r.MessagesRecived).IsRequired()
                .HasForeignKey(p => p.UserReceivedId).HasConstraintName("FK_Message_UserReceived").OnDelete(DeleteBehavior.Restrict);
        }
    }
}
