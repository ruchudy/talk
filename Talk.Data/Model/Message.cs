﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Talk.Data.Views;

namespace Talk.Data.Model
{
    public class Message : IMessageDataView
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;

        public string Group { get; set; }

        public string MessageText { get; set; }

        public int UserSentId { get; set; }

        public int UserReceivedId { get; set; }

        public virtual ApplicationUser UserSent { get; set; }

        public virtual ApplicationUser UserReceived { get; set; }
    }
}
