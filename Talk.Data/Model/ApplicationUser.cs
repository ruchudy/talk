﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using Talk.Data.Views;

namespace Talk.Data.Model
{
    public class ApplicationUser : IdentityUser<int>, IUserDataView
    {
        public virtual ICollection<Message> MessagesSent { get; set; } = new HashSet<Message>();

        public virtual ICollection<Message> MessagesRecived { get; set; } = new HashSet<Message>();
    }
}
