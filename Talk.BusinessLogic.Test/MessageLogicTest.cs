using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Talk.Data.Model;
using Talk.BusinessLogic.Messages;

namespace Talk.BusinessLogic.Test
{
    public class MessageLogicTest
    {
        private readonly MessageLogic _messageService;

        public MessageLogicTest()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();

            var optionsBuilder = new DbContextOptionsBuilder<TalkDbContext>();
            optionsBuilder.UseSqlServer(GetIConfigurationRoot().GetConnectionString("DefaultConnection"));

            _messageService = new MessageLogic(serviceProvider.GetService<ILoggerFactory>(), optionsBuilder.Options);
        }

        public static IConfigurationRoot GetIConfigurationRoot()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true)
                .Build();
        }

        [Fact]
        public void AddMessageTest()
        {
            _messageService.AddMessage("admin", "Ruda", "Test Message");
        }
    }
}
