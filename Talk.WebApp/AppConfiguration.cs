﻿namespace Talk.WebApp
{
    /// <summary>
    /// Nastavení aplikace.
    /// </summary>
    public class AppConfiguration
    {
        /// <summary>
        /// Počet prvků v seznamu pro stránkování.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Počet posledních zpráv k zobrazení.
        /// </summary>
        public int ViewLastMsg { get; set; }
    }
}
