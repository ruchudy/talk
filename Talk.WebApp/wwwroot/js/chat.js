﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chat").build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("ReceiveMessage", function (mine, message, time) {
    var encodedMsg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var item = null;
    if (!mine) {

        item = $('<div>', {
            "class": "incoming_msg",
            html: $('<div>', {
                "class": "received_msg",
                html: [$('<p>', { html: encodedMsg }),
                $('<span>', { "class": "time_date", html: time })]
            })
        });

    } else {

        item = $('<div>', {
            "class": "outgoing_msg",
            html: $('<div>', {
                "class": "sent_msg",
                html: [$('<p>', { html: encodedMsg }),
                $('<span>', { "class": "time_date", html: time })]
            })
        });
    }

    var msgs = $("#msg_list div");
    if (msgs.length > 12)
        msgs.first().remove();

    $("#msg_list").append(item);
    $("#messageInput").val("").focus();
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    if (message.length > 0) {
        connection.invoke("SendMessage", user, message).catch(function (err) {
            return console.error(err.toString());
        });
    }
    event.preventDefault();
});