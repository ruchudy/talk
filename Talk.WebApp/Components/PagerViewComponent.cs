﻿using Talk.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace Talk.WebApp.Components
{
    public class PagerViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(PagingInfo model) => this.View(model);
    }
}
