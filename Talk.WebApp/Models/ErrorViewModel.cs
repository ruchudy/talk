using System;

namespace Talk.WebApp.Models
{
    /// <summary>
    /// Model pro informace o chyb� b�hem zpracov�n�.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Rozli�en� po�adavku - jeho Id.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Zobrazit ��slo po�adavku?
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}