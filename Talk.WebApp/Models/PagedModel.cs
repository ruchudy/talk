﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talk.WebApp.Models
{
    /// <summary>
    /// Model pro zobrazení záznamů typu TItem s pagerem.
    /// </summary>
    public class PagedModel<TItem>
    {
        /// <summary>
        /// Seznam prvků.
        /// </summary>
        public IEnumerable<TItem> Data { get; set; }

        /// <summary>
        /// Informace pro pager.
        /// </summary>
        public PagingInfo Paging { get; set; } = new PagingInfo();

        /// <summary>
        /// Asynchroní načtení dat.
        /// </summary>
        public async Task GetDataAsync(IQueryable<TItem> dataSource, int pageNumber, int pageSize)
        {
            // Validate arguments
            if (dataSource == null) throw new ArgumentNullException(nameof(dataSource));
            if (pageNumber < 1) throw new ArgumentOutOfRangeException(nameof(pageNumber));
            if (pageSize < 1) throw new ArgumentOutOfRangeException(nameof(pageSize));

            // Get number of records
            this.Paging.TotalRecords = await dataSource.CountAsync();
            this.Paging.PageNumber = pageNumber;
            this.Paging.TotalPages = (int)Math.Ceiling(this.Paging.TotalRecords / (float)pageSize);
            this.Paging.PrevPageNumber = pageNumber - 1;
            this.Paging.NextPageNumber = this.Paging.PageNumber == this.Paging.TotalPages ? 0 : pageNumber + 1;

            // Get data
            this.Data = dataSource.Skip(this.Paging.PrevPageNumber * pageSize).Take(pageSize);
        }

        /// <summary>
        /// Načení dat do modelu.
        /// </summary>
        /// <returns>Seznam prvků.</returns>
        public List<TItem> GetData(IQueryable<TItem> dataSource, int pageNumber, int pageSize)
        {
            // Validate arguments
            if (dataSource == null) throw new ArgumentNullException(nameof(dataSource));
            if (pageNumber < 1) throw new ArgumentOutOfRangeException(nameof(pageNumber));
            if (pageSize < 1) throw new ArgumentOutOfRangeException(nameof(pageSize));

            // Get number of records
            this.Paging.TotalRecords = dataSource.Count();
            this.Paging.PageNumber = pageNumber;
            this.Paging.TotalPages = (int)Math.Ceiling(this.Paging.TotalRecords / (float)pageSize);
            this.Paging.PrevPageNumber = pageNumber - 1;
            this.Paging.NextPageNumber = this.Paging.PageNumber == this.Paging.TotalPages ? 0 : pageNumber + 1;

            // Get data
            this.Data = dataSource.Skip(this.Paging.PrevPageNumber * pageSize).Take(pageSize).ToList();
            return (List<TItem>)this.Data;
        }
    }
}
