﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Talk.WebApp.Models.Friend
{
    /// <summary>
    /// Jedna zpráva.
    /// </summary>
    public class MessageViewModel
    {
        /// <summary>
        /// Id zprávy.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Datum a čas vytvoření.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Odeslal.
        /// </summary>
        public int From { get; set; }

        /// <summary>
        /// Obdržel.
        /// </summary>
        public int To { get; set; }

        /// <summary>
        /// Je to moje zpráva ?
        /// </summary>
        public bool IsMine { get; set; }

        /// <summary>
        /// Text zprávy.
        /// </summary>
        public string Text { get; set; }
    }
}
