﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Models.Friend
{
    /// <summary>
    /// Model pro seznam přátel.
    /// </summary>
    public class FriendViewModel
    {
        /// <summary>
        /// Id uživatele.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Uživ. jméno osoby.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Počet zpráv.
        /// </summary>
        public int? CountMessages { get; set; }

        /// <summary>
        /// Počet odeslaných zpráv.
        /// </summary>
        public int? CountSentMessages { get; set; }

        /// <summary>
        /// Počet doručených zpráv.
        /// </summary>
        public int? CountRecivedMessage { get; set; }

        /// <summary>
        /// Poslední odeslaná zpráva.
        /// </summary>
        public int? LastSentMessageId { get; set; }

        /// <summary>
        /// Poslední přijatá zpráva.
        /// </summary>
        public int? LastRecivedMessageId { get; set; }

        /// <summary>
        /// Datum poslední zprávy.
        /// </summary>
        public DateTime? LastMessageDate { get; set; }

        /// <summary>
        /// Text poslední zprávy
        /// </summary>
        public string LastMessageText { get; set; }
    }
}
