﻿using System.ComponentModel.DataAnnotations;

namespace Talk.WebApp.Models.Friend
{
    /// <summary>
    /// Zobrazení uživatelských dat přítele.
    /// </summary>
    public class FriendInfoViewModel
    {
        /// <summary>
        /// Id uživatele.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Uživatelské jméno.
        /// </summary>
        [Display(Name = "Uživatelské jméno")]
        public string Name { get; set; }

        /// <summary>
        /// Emailová adresa.
        /// </summary>
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Je administrátor?
        /// </summary>
        [Display(Name = "Je admin")]
        public bool IsAdmin { get; set; }
    }
}
