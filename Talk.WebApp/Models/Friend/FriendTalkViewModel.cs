﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Models.Friend
{
    /// <summary>
    /// Komu posílám zprávu + seznam posledních zpráv.
    /// </summary>
    public class FriendTalkViewModel
    {
        /// <summary>
        /// Moje jméno.
        /// </summary>
        public string MyName { get; set; }

        /// <summary>
        /// (User)Id přítele.
        /// </summary>
        public int FriendId { get; set; }

        /// <summary>
        /// Jméno přítele.
        /// </summary>
        public string FriendName { get; set; }

        /// <summary>
        /// Sezmam poslední komunikace.
        /// </summary>
        public IList<MessageViewModel> LastMessages { get; set; }
    }
}
