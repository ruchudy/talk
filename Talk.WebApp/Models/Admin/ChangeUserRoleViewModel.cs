﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Models.Admin
{
    /// <summary>
    /// Model pro změnu role uživatele.
    /// </summary>
    public class ChangeUserRoleViewModel
    {
        /// <summary>
        /// Id uživatele.
        /// </summary>
        public int userId { get; set; }

        /// <summary>
        /// Je přidělena role Admin?
        /// </summary>
        public bool asAdmin { get; set; }
    }
}
