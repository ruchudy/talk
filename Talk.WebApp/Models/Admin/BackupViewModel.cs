﻿using System.ComponentModel.DataAnnotations;

namespace Talk.WebApp.Models.Admin
{
    /// <summary>
    /// Model konfigurace záloh.
    /// </summary>
    public class BackupViewModel
    {
        /// <summary>
        /// Zálohovat seznam uživatelů?
        /// </summary>
        [Display(Name = "Záloha uživatelů")]
        public bool BackupUsers { get; set; }

        /// <summary>
        /// Zálohovat zprávy?
        /// </summary>
        [Display(Name = "Záloha zpráv")]
        public bool BackupMessages { get; set; }
    }
}
