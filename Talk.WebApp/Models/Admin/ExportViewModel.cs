﻿using System;
using System.Collections.Generic;
using Talk.Data.Views;

namespace Talk.WebApp.Models.Admin
{
    /// <summary>
    /// Model pro export/zálohu dat.
    /// </summary>
    public class ExportViewModel
    {
        /// <summary>
        /// Seznam uživatelů.
        /// </summary>
        public ICollection<IUserDataView> Users { get; set; }

        /// <summary>
        /// Seznam zpráv.
        /// </summary>
        public ICollection<IMessageDataView> Messages { get; set; }
    }
}
