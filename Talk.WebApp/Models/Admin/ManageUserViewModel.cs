using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;

namespace Talk.WebApp.Models.Admin
{
    /// <summary>
    /// Model pro administraci u�ivatel�.
    /// </summary>
    public class ManageUserViewModel
    {
        /// <summary>
        /// Id u�ivatele.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// U�ivatelsk� jm�no.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Emailov� adresa.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Je administr�tor?
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}
