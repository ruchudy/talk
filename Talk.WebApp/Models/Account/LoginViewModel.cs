﻿using System.ComponentModel.DataAnnotations;

namespace Talk.WebApp.Models.Account
{
    /// <summary>
    /// Model pro přihlášení uživatele.
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Uživatelské jméno.
        /// </summary>
        [Required]
        [Display(Name = "Uživatelské jméno")]
        public string UserName { get; set; }

        /// <summary>
        /// Heslo pro přihlášení.
        /// </summary>
        [Required, DataType(DataType.Password)]
        [Display(Name = "Heslo")]
        public string Password { get; set; }

        /// <summary>
        /// Zapamatovat si uživatele.
        /// </summary>
        public bool RememberMe { get; set; }
    }
}
