﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Models.Account
{
    /// <summary>
    /// Editace dat uživatele.
    /// </summary>
    public class AccountViewModel
    {
        /// <summary>
        /// Uživatelské jméno.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "{0} musí mít alespoň {2} znaků.", MinimumLength = 3)]
        [Display(Name = "Uživatelské jméno")]
        public string Name { get; set; }

        /// <summary>
        /// Emailová adresa.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Nové heslo pro přihlášení.
        /// </summary>
        [StringLength(100, ErrorMessage = "{0} musí mít alespoň {2} znaků.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Aktuální heslo")]
        public string Password { get; set; }

        /// <summary>
        /// Nové heslo pro přihlášení.
        /// </summary>
        [StringLength(100, ErrorMessage = "{0} musí mít alespoň {2} znaků.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nové heslo")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Kontrola nového hesla pro přihlášení.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Nové heslo znovu")]
        [Compare("NewPassword", ErrorMessage = "Heslo a potvrzovací heslo se neshodují.")]
        public string ConfirmNewPassword { get; set; }
    }
}
