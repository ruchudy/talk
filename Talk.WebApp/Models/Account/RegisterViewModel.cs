﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Models.Account
{
    /// <summary>
    /// Registrace nového uživatele.
    /// </summary>
    public class RegisterViewModel
    {
        /// <summary>
        /// Uživatelské jméno.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "{0} musí mít alespoň {2} znaků.", MinimumLength = 3)]
        [Display(Name = "Uživatelské jméno")]
        public string Name { get; set; }

        /// <summary>
        /// Emailová adresa.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Heslo pro přihlášení.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "{0} musí mít alespoň {2} znaků.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Heslo")]
        public string Password { get; set; }

        /// <summary>
        /// Kontrola hesla pro přihlášení.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Heslo znovu")]
        [Compare("Password", ErrorMessage = "Heslo a potvrzovací heslo se neshodují.")]
        public string ConfirmPassword { get; set; }
    }
}
