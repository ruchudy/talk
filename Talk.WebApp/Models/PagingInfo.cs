﻿using System;

namespace Talk.WebApp.Models
{
    /// <summary>
    /// Informace pro pager.
    /// </summary>
    public class PagingInfo
    {
        /// <summary>
        /// Číslo stránky.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Celkový počet stránek.
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Číslo předchozí stránky.
        /// </summary>
        public int PrevPageNumber { get; set; }

        /// <summary>
        /// Číslo následující stránky.
        /// </summary>
        public int NextPageNumber { get; set; }

        /// <summary>
        /// Celkový počet záznamů.
        /// </summary>
        public int TotalRecords { get; set; }
    }
}
