﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;
using Talk.BusinessLogic.Messages;


namespace Talk.WebApp.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();

        private readonly IMessageLogic _messageLogic;

        public ChatHub(IMessageLogic msgLogic)
        {
            this._messageLogic = msgLogic;
        }

        public async Task SendMessage(string userFor, string message)
        {
            if (String.IsNullOrEmpty(message))
                return;

            string who = Context.User.Identity.Name;
            string when = String.Format("{0:HH:mm} | {0:ddd d.M}", DateTime.Now);

            await this._messageLogic.AddMessageAsync(who, userFor, message);

            foreach (var connectionId in _connections.GetConnections(userFor))
                await Clients.Client(connectionId).SendAsync("ReceiveMessage", 1, message, when);

            foreach (var connectionId in _connections.GetConnections(who))
                await Clients.Client(connectionId).SendAsync("ReceiveMessage", null, message, when);
        }

        public override Task OnConnectedAsync()
        {
            string name = Context.User.Identity.Name;

            _connections.Add(name, Context.ConnectionId);

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string name = Context.User.Identity.Name;

            _connections.Remove(name, Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }
    }
}
