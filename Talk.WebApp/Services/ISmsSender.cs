﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Services
{
    /// <summary>
    /// Služba pro odesílání sms.
    /// </summary>
    public interface ISmsSender
    {
        /// <summary>
        /// Pošli sms.
        /// </summary>
        Task SendSmsAsync(string number, string message);
    }
}
