﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talk.WebApp.Services
{
    /// <summary>
    /// Služba pro odeslání emailu.
    /// </summary>
    public interface IEmailSender
    {
        /// <summary>
        /// Pošli email.
        /// </summary>
        Task SendEmailAsync(string email, string subject, string message);
    }
}
