﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Talk.Data.Model;
using Talk.WebApp.Models;
using Talk.WebApp.Models.Friend;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace Talk.WebApp.Controllers
{
    [Route("Friend"), Authorize]
    public class FriendController : Controller
    {
        private readonly ILogger _logger;

        private readonly AppConfiguration _cfg;

        private readonly TalkDbContext _dc;

        private readonly UserManager<ApplicationUser> _userManager;


        // Constructor
        public FriendController(ILoggerFactory loggerFactory, IOptionsSnapshot<AppConfiguration> optionsSnapshot, UserManager<ApplicationUser> userManager, TalkDbContext dc)
        {
            this._logger = loggerFactory.CreateLogger<FriendController>();
            this._cfg = optionsSnapshot.Value;
            this._dc = dc;
            this._userManager = userManager;
        }

        // Actions

        //
        // GET: /Friend/Call
        [HttpGet, Route("Call/{pageNumber:int:min(1)=1}")]
        public async Task<IActionResult> Call(int pageNumber)
        {
            int myUserId = (await GetCurrentUserAsync()).Id;

            var query = this._dc.Users
                .Where(u => u.Id != myUserId)
                .Select(u => new FriendViewModel()
                {
                    UserId = u.Id,
                    Name = u.UserName,

                    CountSentMessages = u.MessagesSent.Count(m => m.UserReceivedId == myUserId),
                    CountRecivedMessage = u.MessagesRecived.Count(m => m.UserSentId == myUserId),

                    LastSentMessageId = u.MessagesSent.Where(m => m.UserReceivedId == myUserId).Max(m => (int?)m.Id),
                    LastRecivedMessageId = u.MessagesRecived.Where(m => m.UserSentId == myUserId).Max(m => (int?)m.Id)
                })
                .OrderByDescending(x => x.LastRecivedMessageId)
                .ThenBy(x => x.Name);

            var model = new PagedModel<FriendViewModel>();
            model.GetData(query, pageNumber, this._cfg.PageSize).ForEach(friend =>
            {
                friend.CountMessages = friend.CountSentMessages + friend.CountRecivedMessage;

                int lastMessageId = friend.LastRecivedMessageId.GetValueOrDefault(0) > friend.LastSentMessageId.GetValueOrDefault(0) ?
                    friend.LastRecivedMessageId.GetValueOrDefault(0) : friend.LastSentMessageId.GetValueOrDefault(0);

                if (lastMessageId != 0)
                {
                    Message message = this._dc.Messages.First(m => m.Id == lastMessageId);
                    friend.LastMessageDate = message.DateCreated;
                    friend.LastMessageText = message.MessageText;
                }
            });

            return this.View(model);
        }

        //
        // GET: /Friend/Talk/{friend}
        [HttpGet, Route("Talk/{id:int:min(1)}")]
        public async Task<IActionResult> Talk(int id)
        {
            var appUser = await GetCurrentUserAsync();
            int myUserId = appUser.Id;

            var model = await this._dc.Users
                .Where(u => u.Id == id)
                .Select(u => new FriendTalkViewModel()
                {
                    MyName = appUser.UserName,
                    FriendId = u.Id,
                    FriendName = u.UserName
                })
                .SingleOrDefaultAsync();

            if (model == null)
                return this.NotFound();

            String group = String.Concat(Math.Min(model.FriendId, myUserId), ':', Math.Max(model.FriendId, myUserId));

            model.LastMessages = await this._dc.Messages
                .Where(m => m.Group.Equals(group))
                .OrderByDescending(m => m.DateCreated)
                .Take(_cfg.ViewLastMsg)
                .Select(m => new MessageViewModel()
                {
                    Id = m.Id,
                    Created = m.DateCreated,
                    From = m.UserSentId,
                    To = m.UserReceivedId,
                    IsMine = m.UserSentId == myUserId,
                    Text = m.MessageText
                })
                .ToListAsync();

            model.LastMessages = model.LastMessages.OrderBy(m => m.Created).ToList();
            return this.View(model);
        }

        //
        // GET: /Friend/Info/{friend}
        [HttpGet, Route("Info/{id:int:min(1)}")]
        public async Task<IActionResult> Info(int id)
        {
            ApplicationUser user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id == id);

            FriendInfoViewModel model = new FriendInfoViewModel();
            if (user != null)
            {
                model.UserId = user.Id;
                model.Name = user.UserName;
                model.Email = user.Email;
                model.IsAdmin = await _userManager.IsInRoleAsync(user, "admin");
            }

            return View(model);
        }

        #region Helpers

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        #endregion
    }
}
