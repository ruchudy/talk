﻿using System.Threading.Tasks;
using Talk.Data.Model;
using Talk.WebApp.Models.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Talk.WebApp.Controllers
{
    /// <summary>
    /// Práce s uživatelským účtem.
    /// </summary>
    [Route("Account")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;

        // Controller
        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ILoggerFactory loggerFactory)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._logger = loggerFactory.CreateLogger<AccountController>();
        }

        // Actions

        //
        // GET: /Account/Register
        [HttpGet, Route("Register")]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost, Route("Register")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Name, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                    // Send an email with this link
                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                    //    "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User created a new account with password.");
                    return RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }

            return View(model);
        }

        //
        // GET: /Account/Login
        [HttpGet, Route("Login")]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost, Route("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = "/")
        {
            if (this.ModelState.IsValid)
            {
                var result = await this._signInManager.PasswordSignInAsync(
                    model.UserName,
                    model.Password,
                    model.RememberMe,
                    lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    return this.LocalRedirect(returnUrl);
                } else
                {
                    this.ModelState.AddModelError(string.Empty, "Přihlášení se nezdařilo");
                }
            }
            return this.View();
        }

        //
        // GET: /Account/Logout
        [HttpGet, Route("Logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await this._signInManager.SignOutAsync();
            return this.MessageView("Odhlášení", "Byli jste úspěšně odhlášeni");
        }

        //
        // GET: /Account/My
        [HttpGet, Route("My")]
        [Authorize]
        public async Task<IActionResult> My()
        {
            var myUser = await GetCurrentUserAsync();
            var model = new AccountViewModel()
            {
                Name = myUser.UserName,
                Email = myUser.Email
            };

            return this.View(model);
        }

        //
        // POST: /Account/My
        [HttpPost, Route("My")]
        [Authorize]
        public async Task<IActionResult> My(AccountViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                // get user object from the storage
                var user = await GetCurrentUserAsync();

                ViewData["Msg"] = null;
                if (!user.UserName.Equals(model.Name) || !user.Email.Equals(model.Email))
                {
                    // change username and email
                    user.UserName = model.Name;
                    user.Email = model.Email;

                    // Persiste the changes
                    await _userManager.UpdateAsync(user);

                    ViewData["Msg"] = "Vaše údaje byly aktualizovány.";
                }

                if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.NewPassword))
                {
                    // change password
                    await _userManager.ChangePasswordAsync(user, model.Password, model.NewPassword);

                    ViewData["Msg"] = string.Concat(ViewData["Msg"], "Vaše heslo bylo změněno.");
                }
            }

            // clear passwords
            model.Password = string.Empty;
            model.NewPassword = string.Empty;
            model.ConfirmNewPassword = string.Empty;

            return this.View(model);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
