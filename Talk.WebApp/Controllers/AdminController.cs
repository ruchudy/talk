﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Talk.Data.Views;
using Talk.Data.Model;
using Talk.BusinessLogic.Messages;
using Talk.WebApp.Models;
using Talk.WebApp.Models.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Talk.WebApp.Controllers
{
    /// <summary>
    /// Administrace
    /// </summary>
    [Route("Admin"), Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly ILogger _logger;

        private readonly AppConfiguration _cfg;

        private readonly UserManager<ApplicationUser> _userManager;

        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly IMessageLogic _messageLogic;


        // Constructor
        public AdminController(ILoggerFactory loggerFactory, IOptionsSnapshot<AppConfiguration> optionsSnapshot,
            UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IMessageLogic msgLogic)
        {
            this._logger = loggerFactory.CreateLogger<AdminController>();
            this._cfg = optionsSnapshot.Value;
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._messageLogic = msgLogic;
        }

        // Actions

        //
        // GET /Admin/Users
        [HttpGet, Route("Users/{pageNumber:int:min(1)=1}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Users(int pageNumber)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
                return View("Error");

            var users = _userManager.Users
                .Where(u => u.Id != user.Id)
                .Select(u => new ManageUserViewModel()
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Email = u.Email,
                    IsAdmin = false
                })
                .OrderBy(u => u.UserName);

            var model = new PagedModel<ManageUserViewModel>();
            model.GetData(users, pageNumber, this._cfg.PageSize).ForEach(m =>
            {
                m.IsAdmin = _userManager.IsInRoleAsync(_userManager.FindByIdAsync(m.Id.ToString()).Result, "admin").Result;
            });

            return View(model);
        }

        //
        // Post /Admin/Set
        [HttpPost, Route("Set")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Set([FromBody] ChangeUserRoleViewModel changeRole)
        {
            if (changeRole != null)
            {
                var user = await _userManager.FindByIdAsync(changeRole.userId.ToString());
                if (user != null)
                {
                    if (changeRole.asAdmin)
                        await _userManager.AddToRoleAsync(user, "admin");
                    else
                        await _userManager.RemoveFromRoleAsync(user, "admin");
                }
            }

            return new EmptyResult();
        }

        //
        // GET /Admin/Backup
        [HttpGet, Route("Backup")]
        [Authorize(Roles = "admin")]
        public IActionResult Backup()
        {
            return View(new BackupViewModel());
        }

        //
        // POST /Admin/Backup
        [HttpPost, Route("Backup")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Backup(BackupViewModel model)
        {
            String fileData = String.Empty;

            if (model != null && (model.BackupUsers || model.BackupMessages))
            {
                ExportViewModel export = new ExportViewModel();
                if (model.BackupUsers)
                    export.Users = await this._messageLogic.GetAllUsersAsync();

                if (model.BackupMessages)
                    export.Messages = await this._messageLogic.GetAllMessagesAsync();

                fileData = JsonConvert.SerializeObject(export, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    Formatting = Formatting.None
                });
            }

            if (fileData.Length <= 0)
                return View(model);
            else
                return File(Encoding.UTF8.GetBytes(fileData), "text/plain", String.Format("backup{0:yyyyMMddHHmmss}.txt", DateTime.Now));
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        #endregion
    }
}
