﻿CREATE TABLE [dbo].[Messages] (
    [Id]             INT             IDENTITY (1, 1) NOT NULL,
    [Created]        DATETIME2 (7)   NOT NULL,
    [Group]          VARCHAR (25)    NOT NULL,
    [Text]           NVARCHAR (1024) NOT NULL,
    [UserSentId]     INT             NOT NULL,
    [UserReceivedId] INT             NOT NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Message_UserReceived] FOREIGN KEY ([UserReceivedId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Message_UserSent] FOREIGN KEY ([UserSentId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Messages_UserReceivedId]
    ON [dbo].[Messages]([UserReceivedId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Messages_UserSentId]
    ON [dbo].[Messages]([UserSentId] ASC);

