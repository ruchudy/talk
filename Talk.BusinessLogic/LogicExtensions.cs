﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Talk.BusinessLogic.Messages;

namespace Talk.BusinessLogic
{
    /// <summary>
    /// Rozšíření pro BusinessLogic.
    /// </summary>
    public static class LogicExtensions
    {
        /// <summary>
        /// Registruj všechny BusinessLogic objekty.
        /// </summary>
        public static void AddTalkLogic(this IServiceCollection services)
        {
            services.AddScoped<IMessageLogic, MessageLogic>();
        }
    }
}
