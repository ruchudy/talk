﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Talk.Data.Views;
using Talk.Data.Model;

namespace Talk.BusinessLogic.Messages
{
    /// <summary>
    /// Implemencase logiky IMessageLogic.
    /// </summary>
    public class MessageLogic : IMessageLogic
    {
        private readonly ILogger _logger;

        private readonly DbContextOptions<TalkDbContext> _dbOptions;

        public MessageLogic(ILoggerFactory loggerFactory, DbContextOptions<TalkDbContext> dbOptions)
        {
            this._logger = loggerFactory.CreateLogger<MessageLogic>();
            this._dbOptions = dbOptions;
        }

        /// <summary>
        /// Ulož novou zprávu.
        /// </summary>
        public Task AddMessageAsync(string userWho, string userFor, string message)
        {
            return Task.Run(() => AddMessage(userWho, userFor, message));
        }

        public void AddMessage(string userWho, string userFor, string message)
        {
            if (String.IsNullOrEmpty(userWho)) throw new ArgumentNullException("userWho");
            if (String.IsNullOrEmpty(userFor)) throw new ArgumentNullException("userFor");
            if (String.IsNullOrEmpty(message)) throw new ArgumentNullException("message");

            using (var db = new TalkDbContext(this._dbOptions))
            {
                int userIdWho = db.Users.Single(u => u.UserName.Equals(userWho)).Id;
                int userIdFor = db.Users.Single(u => u.UserName.Equals(userFor)).Id;

                string group = String.Concat(Math.Min(userIdWho, userIdFor), ':', Math.Max(userIdWho, userIdFor));

                Message msg = new Message()
                {
                    Group = group,
                    MessageText = message,
                    UserSentId = userIdWho,
                    UserReceivedId = userIdFor
                };

                db.Messages.Add(msg);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Načte všechny uživatele, kteří komunikují.
        /// </summary>
        /// <returns>Seznam uživatelů.</returns>
        public Task<List<IUserDataView>> GetAllUsersAsync()
        {
            return Task.Run(() => GetAllUsers());
        }

        public List<IUserDataView> GetAllUsers()
        {
            using (var db = new TalkDbContext(this._dbOptions))
            {
                return db.Users
                   .Where(u => u.MessagesRecived.Any() || u.MessagesSent.Any())
                   .Select(u => new UserDataView()
                   {
                       Id = u.Id,
                       UserName = u.UserName,
                       Email = u.Email
                   })
                   .Cast<IUserDataView>()
                   .ToList();
            }
        }

        /// <summary>
        /// Načte všechny zprávy.
        /// </summary>
        /// <returns>Seznam zpráv.</returns>
        public Task<List<IMessageDataView>> GetAllMessagesAsync()
        {
            return Task.Run(() => GetAllMessages());
        }

        public List<IMessageDataView> GetAllMessages()
        {
            using (var db = new TalkDbContext(this._dbOptions))
            {
                return db.Messages
                    .Select(m => new MessageDataView()
                    {
                        Id = m.Id,
                        DateCreated = m.DateCreated,
                        MessageText = m.MessageText,
                        UserSentId = m.UserSent.Id,
                        UserReceivedId = m.UserReceived.Id
                    })
                    .OrderByDescending(m => m.DateCreated)
                    .Cast<IMessageDataView>()
                    .ToList();
            }
        }
    }
}
