﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Talk.Data.Views;

namespace Talk.BusinessLogic.Messages
{
    /// <summary>
    /// Logika nad zprávami.
    /// </summary>
    public interface IMessageLogic
    {
        /// <summary>
        /// Ulož novou zprávu.
        /// </summary>
        Task AddMessageAsync(string userWho, string userFor, string message);

        /// <summary>
        /// Seznam všech uživatelů, kteří komunikovali.
        /// </summary>
        /// <returns>Seznam uživatelů.</returns>
        Task<List<IUserDataView>> GetAllUsersAsync();

        /// <summary>
        /// Seznam všech zpráv.
        /// </summary>
        /// <returns>Seznam zpráv.</returns>
        Task<List<IMessageDataView>> GetAllMessagesAsync();
    }
}
